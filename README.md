# slowcat

> **(May 2021)** moved to [md0.org/slowcat](https://md0.org/slowcat).

cat, with a configurable lag

## limitations

- `slowcat` only reads from stdin.
- certainly more!

## usage

Default delay is 100ms.  To set it to, say, 500ms do:

```
slowcat -d 500 < file # or redirect however you want to!
```
