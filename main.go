package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

const VERSION = "1.0.1"

func main() {
	var (
		d = flag.Int("d", 100, "delay in milliseconds")
		v = flag.Bool("v", false, "print short version")
	)
	flag.Parse()

	if *v {
		fmt.Println(VERSION)
		return
	}

	delay := time.Duration(*d)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
		time.Sleep(delay * time.Millisecond)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
